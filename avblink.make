core = 7.x
api = 2

; Contrib
projects[admin_menu][subdir] = "contrib"
;projects[admin_menu][version] = ""

projects[module_filter][subdir] = "contrib"
;projects[module_filter][version] = ""

projects[ctools][subdir] = "contrib"
;projects[ctools][version] = ""

projects[devel][subdir] = "contrib"
;projects[devel][version] = ""

projects[entity][subdir] = "contrib"
;projects[entity][version] = ""

projects[entity_view_mode][subdir] = "contrib"
;projects[entity_view_mode][version] = ""

projects[features][subdir] = "contrib"
;projects[features][version] = ""

projects[rules][subdir] = "contrib"
;projects[rules][version] = ""

projects[token][subdir] = "contrib"
;projects[token][version] = ""

projects[strongarm][subdir] = "contrib"
;projects[strongarm][version] = ""

projects[jquery_update][subdir] = "contrib"
;projects[jquery_update][version] = ""

projects[search_api][subdir] = "contrib"
;projects[search_api][version] = ""

projects[flag][subdir] = "contrib"
;projects[flag][version] = ""

projects[globalredirect][subdir] = "contrib"
;projects[globalredirect][version] = ""

projects[pathauto][subdir] = "contrib"
;projects[pathauto][version] = ""

projects[page_title][subdir] = "contrib"
;projects[page_title][version] = ""

projects[panels][subdir] = "contrib"
;projects[panels][version] = ""

projects[webform][subdir] = "contrib"
;projects[webform][version] = ""

projects[views][subdir] = "contrib"
;projects[views][version] = ""


; Custom

;projects[high_contrast][type] = "module"
;projects[high_contrast][download][type] = "git"
;projects[high_contrast][download][url] = "http://git.drupal.org/sandbox/acontia/1281604.git"
;projects[high_contrast][subdir] = "custom"
